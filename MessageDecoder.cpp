#include "MessageDecoder.h"

#include <sstream>
#include <cassert>

CMessageDecoder::CMessageDecoder(const std::string& m)
  : mess{ m }
  , valid{ false }
  , type{ EMessageType::eInvalid }
  , value{ 0 }
  , operation{ EOperation::eNone }
{
  decode();
}

bool CMessageDecoder::isValid() const
{
  return valid;
}

EMessageType CMessageDecoder::getType() const
{
  return type;
}

int CMessageDecoder::getValue() const
{
  return value;
}

EOperation CMessageDecoder::getOperation() const
{
  return operation;
}

std::string CMessageDecoder::getErrorMessage() const
{
  return errorMessage;
}

void CMessageDecoder::decode()
{
  // TODO: implement
}


